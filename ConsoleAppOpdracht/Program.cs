﻿using System;
using System.Threading;

namespace ConsoleApp
{
    /// <summary>
    ///     <para>ConsoleApp Opdracht, ADSD20A1 periode3, SoftwareDevelopment1</para>
    ///     <para>Print a menu and let the user choose a action</para>
    /// </summary>
    internal class Program
    {
        /// <summary>
        ///     The main method where the app start and show a menu
        /// </summary>
        private static void Main()
        {
            Console.Title = "ConsoleApp team A1";

            string[] title =
            {
                "ADSD 2020 team A1 - Periode 3",
                "Software development"
            };
            TextAnimation.Animate(title);


            // make a instance of menu class
            var menu = new Menu();

            //endless loop de method ShowMenu of menu object
            while (true) menu.ShowMenu();
        }
    }
}