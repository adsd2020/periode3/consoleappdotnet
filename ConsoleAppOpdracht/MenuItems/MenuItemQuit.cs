﻿using System;
using ConsoleApp.MenuItems;

namespace ConsoleApp.MenuItem
{
    /// <summary>
    ///     The menu item to quit the program
    /// </summary>
    internal class MenuItemQuit : IMenuItem
    {
        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Number" />
        /// </summary>
        public int Number => 4;

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Title" />
        /// </summary>
        public string Title => "Quit";

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Execute" />
        /// </summary>
        public void Execute()
        {
            string[] teamMembers =
            {
                "Team members:",
                "",
                "Joeghanoe Bhatti",
                "Niels Horeman",
                "Alec Hoefsmid",
                "Renas Khalil",
                "Ali Saviz"
            };

            ConsoleColor[] lineColors = new[]
            {
                ConsoleColor.DarkGreen,
                ConsoleColor.DarkGreen,
                ConsoleColor.Blue,
                ConsoleColor.Blue,
                ConsoleColor.Blue,
                ConsoleColor.Blue,
                ConsoleColor.Blue
            };

            TextAnimation.Animate(teamMembers,textColors: lineColors);

            // Quits the program
            Environment.Exit(0);
        }
    }
}