﻿using System;

namespace ConsoleApp.MenuItems
{
    /// <summary>
    ///     <para>Third menu item to show the saved name.</para>
    ///     Implementation of:
    ///     <inheritdoc cref="IMenuItem" />
    /// </summary>
    internal class MenuIemGetName : IMenuItem
    {
        /// <summary>
        ///     Extra property as person object
        /// </summary>
        private readonly Person _person;

        /// <summary>
        ///     Constructor of the class that received the person (by ref)
        /// </summary>
        /// <param name="person"></param>
        public MenuIemGetName(Person person)
        {
            _person = person;
        }

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Number" />
        /// </summary>
        public int Number => 3;

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Title" />
        /// </summary>
        public string Title => "Show the saved name";

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Execute" />
        /// </summary>
        public void Execute()
        {
            //clear de terminal
            Console.Clear();

            //control if the PersonName of this object empty is
            if (string.IsNullOrEmpty(_person.Name))
                //show a message that no name is set
                Console.WriteLine("No name inserted yet. \nInsert first the name through first menu option.");
            else
                //show the PersonName property of this object
                Console.WriteLine($"The saved name is {_person.Name}");

            //wait until user pressed a key to continue
            Console.WriteLine("\nPress ENTER to return to the menu");
            Console.ReadKey();

            //clear de terminal
            Console.Clear();
        }
    }
}