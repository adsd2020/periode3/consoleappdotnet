﻿using System;

namespace ConsoleApp.MenuItems
{
    /// <summary>
    ///     <para>First menu item to ask and save a name.</para>
    ///     Implementation of:
    ///     <inheritdoc cref="IMenuItem" />
    /// </summary>
    internal class MenuItemAskName : IMenuItem
    {
        /// <summary>
        ///     Extra property as person object
        /// </summary>
        private readonly Person _person;

        /// <summary>
        ///     Constructor of the class that received the person (by ref)
        /// </summary>
        /// <param name="person"></param>
        public MenuItemAskName(Person person)
        {
            _person = person;
        }

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Number" />
        /// </summary>
        public int Number => 1;

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Title" />
        /// </summary>
        public string Title => "Save a name";

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Execute" />
        /// </summary>
        public void Execute()
        {
            //Clear the console.
            Console.Clear();

            //Ask user to insert a name .
            Console.WriteLine("Insert a name:");

            //Read the inserted line and put it in the UserName property.
            _person.Name = Console.ReadLine();

            //clear de terminal
            Console.Clear();
        }
    }
}