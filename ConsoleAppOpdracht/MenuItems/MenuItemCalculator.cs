﻿using System;
using ConsoleApp.MenuItems;

namespace ConsoleApp.MenuItem
{
    /// <summary>
    ///     The menu item to add numbers
    /// </summary>
    internal class MenuItemCalculator : IMenuItem
    {
        public int Number => 2;

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Title" />
        /// </summary>
        public string Title => "Calculator";

        /// <summary>
        ///     <inheritdoc cref="IMenuItem.Execute" />
        /// </summary>
        public void Execute()
        {
            // Declare variables and set to empty.
            var firstInput = "";
            var secondInput = "";
            double result = 0;

            // Ask the user to type the first number.
            Console.Clear();
            Console.Write("Enter your first number: ");
            firstInput = Console.ReadLine();

            double cleanFirstInput = 0;
            // Check if input is integer
            while (!double.TryParse(firstInput, out cleanFirstInput))
            {
                Console.Write("This is not valid input. Please enter an integer value: ");
                firstInput = Console.ReadLine();
            }

            // Ask the user to type the second number.
            Console.Write("Enter your second number: ");
            secondInput = Console.ReadLine();

            double cleanSecondInput = 0;
            // Check if input is integer
            while (!double.TryParse(secondInput, out cleanSecondInput))
            {
                Console.Write("This is not valid input. Please enter an integer value: ");
                secondInput = Console.ReadLine();
            }

            // Ask the user to choose an operator.
            Console.WriteLine("Choose an operator from the following list:");
            Console.WriteLine("\ta - Add");
            Console.WriteLine("\ts - Subtract");
            Console.WriteLine("\tm - Multiply");
            Console.WriteLine("\td - Divide");
            Console.Write("Your option? ");

            var option = Console.ReadLine();
            // Check if a valid option is chosen
            while (option != "a" && option != "s" && option != "m" && option != "d")
            {
                Console.Write("Please enter one of the given options: ");
                option = Console.ReadLine();
            }

            // Try the operation function in the Calculator class
            try
            {
                result = Calculator.operation(cleanFirstInput, cleanSecondInput, option);
                if (double.IsNaN(result))
                    Console.WriteLine("This operation will result in a mathematical error.\n");
                else Console.WriteLine("Your result: {0:0.##}\n", result);
            }
            // Catch error if it fails
            catch (Exception e)
            {
                Console.WriteLine("Oh no! An exception occurred trying to do the math.\n - Details: {0}", e.Message);
            }

            Console.WriteLine("Press ENTER to return to the menu");
            Console.ReadKey();
        }
    }

    internal class Calculator
    {
        public static double operation(double first, double second, string option)
        {
            var result = double.NaN;

            switch (option)
            {
                case "a":
                    result = first + second;
                    break;
                case "s":
                    result = first - second;
                    break;
                case "m":
                    result = first * second;
                    break;
                case "d":
                    // Ask the user to enter a non-zero divisor.
                    while (second == 0)
                    {
                        Console.WriteLine("Enter a non-zero divisor: ");
                        second = Convert.ToInt32(Console.ReadLine());
                    }

                    result = first / second;
                    break;
            }

            return result;
        }
    }
}