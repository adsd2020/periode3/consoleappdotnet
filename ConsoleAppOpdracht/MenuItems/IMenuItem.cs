﻿namespace ConsoleApp.MenuItems
{
    /// <summary>
    /// The interface of the menu items
    /// </summary>
    internal interface IMenuItem
    {
        /// <summary>
        /// The number of menu item, which user can choose.
        /// </summary>
        int Number { get; }

        /// <summary>
        /// The title which show to user.
        /// </summary>
        string Title { get; }

        /// <summary>
        /// The action that this menu iem does.
        /// </summary>
        void Execute();
    }
}