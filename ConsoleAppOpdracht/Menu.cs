﻿using System;
using System.Linq;
using ConsoleApp.MenuItem;
using ConsoleApp.MenuItems;

namespace ConsoleApp
{
    /// <summary>
    ///     <para>menu object</para>
    ///     set, show, and execute the action of menu items.
    /// </summary>
    internal class Menu
    {
        // Define a array variable for menu items
        private readonly IMenuItem[] _menuItems;

        // Make a private instance of Person
        private readonly Person _person = new Person();

        /// <summary>
        ///     constructor of class to put the menu items in items array
        /// </summary>
        public Menu()
        {
            //Set the menuItems variable.
            _menuItems = new IMenuItem[]
            {
                new MenuItemAskName(_person),
                new MenuItemCalculator(),
                new MenuIemGetName(_person),
                new MenuItemQuit()
            };
        }

        /// <summary>
        ///     the method to show the menu
        /// </summary>
        public void ShowMenu()
        {
            // Clear console
            Console.Clear();

            // Loop on menuItems array and print number and title of each menu iem
            foreach (var menuItem in _menuItems) Console.WriteLine(menuItem.Number + ". " + menuItem.Title);

            // Read the inserted key by user
            var selectedKey = Console.ReadKey().KeyChar.ToString();
            
            // Try to convert inserted ket to integer and set the slectedNumber integer, else go back.
            if (!int.TryParse(selectedKey, out var selectedNumber)) return;

            //If can find the class in the array run its execute method
            _menuItems.First(_item => _item.Number == selectedNumber)?.Execute();
        }
    }
}