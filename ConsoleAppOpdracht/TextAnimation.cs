﻿using System;
using System.Threading;

namespace ConsoleApp
{
    /// <summary>
    /// A static class to get text animation effect
    /// </summary>
    public static class TextAnimation
    {
        /// <summary>
        /// static method to type sentences per character in the middle of screen
        /// </summary>
        /// <param name="text">An array of sentences. each sentence one item of array</param>
        /// <param name="animationSpeed"></param>
        /// <param name="textColors">an array of ConsolColor objects</param>
        public static void Animate(string[] text, int animationSpeed = 50, ConsoleColor[] textColors = null)
        {
            // a variable to take a color per line
            var colorIndex = 0;

            // if text color is not passed to method, take automatic dark green.
            textColors = textColors ?? new[] {ConsoleColor.DarkGreen};

            //Remove screen and make cursor invisible.
            Console.Clear();
            Console.CursorVisible = false;


            for (var i = 0; i < text.Length; i++)
            {
                if (colorIndex > textColors.Length - 1) colorIndex = 0;
                Console.ForegroundColor = textColors[colorIndex];
                colorIndex++;


                Console.SetCursorPosition((Console.WindowWidth - text[i].Length) / 2,
                    Console.WindowHeight / 2 - text.Length + 1 + i);

                for (var j = 0; j < text[i].Length; j++)
                {
                    Console.Write(text[i].Substring(text[i].Length - j - 1));
                    Thread.Sleep(animationSpeed);
                    Console.SetCursorPosition(Console.CursorLeft - j - 1, Console.CursorTop);
                }
            }

            Thread.Sleep(1000);
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.CursorVisible = true;
        }
    }
}