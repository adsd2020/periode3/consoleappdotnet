﻿namespace ConsoleApp
{
    /// <summary>
    /// <para>Person object to save the name.</para>
    /// <para>Made as an object, want if is needed, can later have more methods and properties.</para>
    /// </summary>
    internal class Person
    {
        /// <summary>
        /// The name of person
        /// </summary>
        public string Name;
    }
}